package org.harouna.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "t_instrument")
public class Instrument {




	@Id @GeneratedValue(strategy = GenerationType.AUTO) //G�n�re automatiquement d'id 
	private long id;
	
	@Column(name ="instrument_name", length = 64)
	private String name;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "instrument_type", length = 32)
	private InstrumentType instrumentType;
	
	@OneToOne(mappedBy="instrument") //musician pointe vers instrument et vice versa, relations bidirectionnelles!!
	private Musician musician;
	
	public Musician getMusician() {
		return musician;
	}


	public void setMusician(Musician musician) {
		this.musician = musician;
	}


	public InstrumentType getInstrumentType() {
		return instrumentType;
	}


	public void setInstrumentType(InstrumentType instrumentType) {
		this.instrumentType = instrumentType;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Instrument [id=" + id + ", name=" + name + ", instrumentType=" + instrumentType + "]";
	}

}

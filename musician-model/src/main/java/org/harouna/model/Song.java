package org.harouna.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.persistence.jpa.config.Cascade;

@Entity
@Table(name="t_song")
public class Song {
	
	@Override
	public String toString() {
		return "Song [id=" + id + ", name=" + name + "]";
	}

	@Id @GeneratedValue(strategy = GenerationType.AUTO) //G�n�re automatiquement d'id 
	@Column(name= "id")
	private long id;
	
	@Column(name ="song_name")
	private String name;
	
	@ManyToOne(cascade = CascadeType.PERSIST,
			fetch= FetchType.EAGER)						//Inverse d une relation OneToMany
	private Musician musician; 
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Musician getMusician() {
		return musician;
	}

	public void setMusician(Musician musician) {
		this.musician = musician;
	}
	
}
